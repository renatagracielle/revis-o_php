<?php include_once 'inc/cabecalho.php'; ?>


<form class="form-horizontal" method="post" action="gravar.php">
<fieldset>

<!-- Form Name -->
<legend>Cadastro do Bem</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nome</label>  
  <div class="col-md-5">
  <input id="textinput" name="nom_bem" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Texto da Home</label>  
  <div class="col-md-5">
  <input id="textinput" name="home" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>

<!-- Prepended text-->
<div class="form-group">
  <label class="col-md-4 control-label" for="prependedtext">Valor 1 Praça</label>
  <div class="col-md-4">
    <div class="input-group">
      <span class="input-group-addon">R$</span>
      <input id="prependedtext" name="valor" class="form-control" type="text">
    </div>
  </div>
</div>



<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="selectbasic">Deseja Exibir</label>
  <div class="col-md-5">
    <select id="selectbasic" name="mostrar" class="form-control">
      <option value="1">Option one</option>
      <option value="2">Option two</option>
    </select>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for=""></label>
  <div class="col-md-4">
      <button id="" name="" type="submit" class="btn btn-success">Enviar</button>
  </div>
</div>





</fieldset>
</form>

        
  <?php include_once 'inc/rodape.php'; ?>